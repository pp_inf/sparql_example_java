package pl.pp;

import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.*;
import org.apache.jena.rdfconnection.RDFConnection;
import org.apache.jena.rdfconnection.RDFConnectionFactory;

public class Main {
    private static final String QUERY_SELECT = "SELECT DISTINCT ?s ?p ?o { ?s ?p ?o }";
    private static final String QUERY_INSERT = "PREFIX oa: <http://www.w3.org/ns/oa#> PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> INSERT DATA{ [ a oa:Annotation ; rdfs:label \"Title\" ; ] .}";

    public static void main(String[] args) {
        RDFConnection conn = RDFConnectionFactory.connect("http://145.239.82.37:3030/testDB/");

        try {
            PerformSelectQuery(conn, QUERY_SELECT);
//            PerformInsertQuery(conn, QUERY_INSERT);
        }
        catch (Exception ex){
            System.out.println(ex.getMessage());
        }

        conn.close() ;
    }

    private static void PerformSelectQuery(RDFConnection conn, String query){
        QueryExecution qExec = conn.query(query) ;
        ResultSet rs = qExec.execSelect() ;
        while(rs.hasNext()) {
            QuerySolution qs = rs.next() ;
            PrintResults(qs);
        }
        qExec.close() ;
    }

    private static void PrintResults(QuerySolution qs){
        RDFNode subject = qs.get("s") ;
        RDFNode predicate = qs.get("p") ;
        RDFNode object = qs.get("o") ;
        System.out.println(subject.toString() + " " +  predicate.toString() + " " + object.toString()) ;
    }

    private static void PerformInsertQuery(RDFConnection conn, String query){
        conn.update(query);
    }
}
